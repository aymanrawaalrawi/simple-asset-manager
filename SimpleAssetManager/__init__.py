# Addon Info
from bpy.types import WindowManager
import bpy.utils.previews
from bpy.props import *
import bpy
import bmesh
import re
import os
from math import pi, cos, sin, radians
from mathutils import Euler

bl_info = {
    "name": "Simple Asset Manager",
    "description": "Manager for objects, materials, particles, hdr. Before official.",
    "author": "Dawid Huczyński",
    "version": (0, 6),
    "blender": (2, 80, 0),
    "location": "View 3D > Properties",
    "wiki_url": "https://gitlab.com/tibicen/simple-asset-manager",
    "tracker_url": "",
    "support": "OFFICIAL",
    "category": "Import-Export"
}

# TODO: import as Collections
# TODO: add licence info
# TODO: automatic materials from blend file,
#       separate and save in materials folder
# TODO: fix silent error on empty asset_manager_prevs, anyone?!

FORMATS = ('.blend', '.obj', '.fbx', '.hdr', '.exr')


def purge(data):
    # RENDER PREVIEW SCENE PREPARATION METHODS
    for el in data:
        if el.users == 0:
            data.remove(el)


def prepare_scene(blendFile):
    # clean scene
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete(use_global=True)
    purge(bpy.data.objects)
    purge(bpy.data.particles)
    purge(bpy.data.materials)
    purge(bpy.data.textures)
    purge(bpy.data.images)
    # set output
    eevee = bpy.context.scene.eevee
    render = bpy.context.scene.render
    eevee.use_ssr_refraction = True
    eevee.use_ssr = True
    eevee.use_gtao = True
    eevee.gtao_distance = 1
    render.filepath = os.path.splitext(blendFile)[0]
    render.stamp_note_text = os.path.splitext(blendFile)[1][1:].upper()
    render.alpha_mode = 'TRANSPARENT'
    render.resolution_x = 200
    render.resolution_y = 200
    render.use_stamp_date = False
    render.use_stamp_render_time = False
    render.use_stamp_camera = False
    render.use_stamp_scene = False
    render.use_stamp_filename = False
    render.use_stamp_frame = False
    render.use_stamp_time = False
    render.use_stamp = True
    render.use_stamp_note = True
    render.stamp_font_size = 20


def add_camera():
    bpy.ops.object.camera_add(rotation=(pi / 2, 0, -pi / 6))
    cam = bpy.context.active_object
    cam.data.shift_y = -.3
    cam.data.lens = 71
    bpy.data.scenes[0].camera = cam
    bpy.ops.view3d.camera_to_view_selected()
    return cam


def append_element(blendFile, el_type, link=False):
    assert el_type in ['Object', 'Material', 'ParticleSettings', 'HDR']
    files = []
    for ob in bpy.context.scene.objects: ob.select_set(False)
    if blendFile.endswith('.obj'):
        bpy.ops.import_scene.obj(filepath=blendFile)
    elif blendFile.endswith('.fbx'):
        bpy.ops.import_scene.fbx(filepath=blendFile)
    elif blendFile.endswith(('.hdr', '.exr')):
        file = os.path.basename(blendFile)
        # check if already loaded
        if file in bpy.data.worlds.keys():
            world = bpy.data.worlds[file]
        else:
            bpy.ops.image.open(filepath=blendFile)
            im = bpy.data.images[file]
            world = bpy.data.worlds.new(file)
            world.use_nodes = True
            nodes = world.node_tree.nodes
            tex = nodes.new('ShaderNodeTexEnvironment')
            tex.image = im
            background = nodes['Background']
            world.node_tree.links.new(background.inputs['Color'],
                                      tex.outputs['Color'])
        bpy.context.scene.world = world
    elif blendFile.endswith('.blend'):
        with bpy.data.libraries.load(blendFile) as (data_from, data_to):
            if el_type == 'Object':
                for name in data_from.objects:
                    files.append({'name': name})
            elif el_type == 'Material':
                for name in data_from.materials:
                    files.append({'name': name})
            elif el_type == 'ParticleSettings':
                for name in data_from.particles:
                    files.append({'name': name})

        action = bpy.ops.wm.link if link else bpy.ops.wm.append
        action(directory=blendFile + f"/{el_type}/",
               files=files)
        return files


def append_hdr(blendFile, cam):
    bpy.ops.mesh.primitive_uv_sphere_add(segments=64,
                                         ring_count=32,
                                         location=(0, 0, 0))
    bpy.ops.object.shade_smooth()
    bpy.ops.object.material_slot_add()
    bpy.ops.material.new()
    ob = bpy.context.active_object
    mat = bpy.data.materials[-1]
    ob.material_slots[0].material = mat
    shader = mat.node_tree.nodes['Principled BSDF']
    shader.inputs['Metallic'].default_value = 1
    shader.inputs['Roughness'].default_value = 0
    append_element(blendFile, 'HDR')
    cam.rotation_euler = Euler((pi / 2, 0, 0), 'XYZ')
    cam.data.shift_y = 0
    cam.data.lens = 41


def rot_point(point, angle):
    angle = radians(angle)
    x, y = point
    rx = x*cos(angle) - y*sin(angle)
    ry = x*sin(angle) + y*cos(angle)
    return (rx, ry)


def rotate_uv(ob, angle):
    UV = ob.data.uv_layers[0]
    for v in ob.data.loops:
        UV.data[v.index].uv = rot_point(UV.data[v.index].uv, angle)


def append_material(blendFile, cam):
    bpy.ops.mesh.primitive_uv_sphere_add(segments=64,
                                         ring_count=32,
                                         location=(0, 0, 0))
    ob = bpy.context.active_object
    bpy.ops.object.editmode_toggle()
    bpy.ops.uv.sphere_project(direction='ALIGN_TO_OBJECT')
    mesh = bmesh.from_edit_mesh(ob.data)
    for v in mesh.verts:
        v.select = True if v.co[1] < 0 else False
    bmesh.update_edit_mesh(ob.data)
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.uv.unwrap(method='CONFORMAL', margin=0)
    # bpy.ops.uv.pack_islands()
    bpy.ops.mesh.select_all(action='INVERT')
    bpy.ops.uv.unwrap(method='CONFORMAL', margin=0)
    # bpy.ops.uv.pack_islands()
    bpy.ops.object.editmode_toggle()
    rotate_uv(ob, 46.5)
    bpy.ops.object.shade_smooth()
    bpy.ops.object.material_slot_add()
    # append_material
    files = append_element(blendFile, 'Material')
    name = files[0]['name']
    mat = bpy.data.materials[name]
    ob.material_slots[0].material = mat
    # modify camera settings
    cam.rotation_euler = Euler((pi / 2, 0, 0), 'XYZ')
    cam.data.shift_y = 0
    cam.data.lens = 41


def append_particle_settings(blendFile):
    bpy.ops.mesh.primitive_uv_sphere_add(segments=64,
                                         ring_count=32,
                                         enter_editmode=True,
                                         location=(0, 0, 0))
    bpy.ops.uv.sphere_project()
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.shade_smooth()
    sphere = bpy.context.active_object
    files = append_element(blendFile, 'ParticleSettings')
    for f in files:
        name = f['name']
        bpy.ops.object.particle_system_add()
        settings = bpy.data.particles[name]
        sphere.particle_systems[-1].settings = settings
        # for render preview
        settings.child_nbr = settings.rendered_child_count
    bpy.ops.object.select_all(action='DESELECT')
    for ob in bpy.data.objects:
        if ob.type == 'CAMERA' or ob == sphere:
            pass
        else:
            ob.select_set(True)
    bpy.context.view_layer.objects.active = bpy.context.selected_objects[0]
    bpy.ops.object.move_to_collection(collection_index=0,
                                      is_new=True,
                                      new_collection_name="hide")
    bpy.data.collections[-1].hide_viewport = True
    sphere.select_set(True)
    # bpy.ops.object.editmode_toggle()
    # bpy.ops.object.editmode_toggle()
    sphere.particle_systems[-1].seed = 1


class SAM_render_previews(bpy.types.Operator):
    bl_idname = "asset_manager.render_previews"
    bl_label = "(re)Render all previews"

    def execute(self, context):
        # TODO: when render prev, clean prev collection so they can be load again
        pcoll = preview_collections["main"]
        pcoll.asset_manager_prev_dir = ""
        pcoll.asset_manager_prevs = ""
        pref = context.preferences.addons[__name__].preferences
        lib_path = pref.lib_path
        rerender = pref.rerender
        for r, d, fs in os.walk(lib_path):
            for f in fs:
                render_type = 'RENDERED' if f.endswith(('.hdr', '.exr')) else 'MATERIAL'
                if f.endswith(FORMATS):
                    blendFile = os.path.join(r, f)
                    png = os.path.splitext(blendFile)[0] + '.png'
                    if not rerender and os.path.exists(png):
                        continue
                    prepare_scene(blendFile)
                    cam = add_camera()
                    if f.endswith(('.hdr', '.exr')):
                        continue
                        # TODO: render previews like texture haven
                        # append_hdr(blendFile, cam)
                    elif 'material' in r.lower():
                        append_material(blendFile, cam)
                    elif 'particle' in r.lower():
                        append_particle_settings(blendFile)
                    elif 'node' in r.lower():
                        pass
                    else:
                        append_element(blendFile, 'Object')
                    bpy.ops.object.select_all(action='SELECT')
                    bpy.ops.view3d.camera_to_view_selected()
                    cam.data.lens = 40 if 'material' in r.lower() else 70
                    for area in bpy.context.screen.areas:
                        area.type = 'VIEW_3D'
                        area.spaces[0].region_3d.view_perspective = 'CAMERA'
                        area.spaces[0].shading.type = render_type
                        area.spaces[0].overlay.show_overlays = False
                        if render_type == 'MATERIAL':
                            area.spaces[0].shading.studio_light = 'noon.exr'
                    bpy.ops.render.opengl(write_still=True)
        return{'FINISHED'}


def execute_insert(context, link):
    selected_preview = bpy.data.window_managers["WinMan"].asset_manager_prevs
    # Append objects
    folder = os.path.split(os.path.split(selected_preview)[0])[1]
    bpy.ops.object.select_all(action='DESELECT')
    if 'material' in folder.lower():
        append_element(selected_preview, 'Material', link)
    elif 'particle' in folder.lower():
        append_element(selected_preview, 'ParticleSettings', link)
    elif selected_preview.endswith(('.hdr', '.exr')):
        append_element(selected_preview, 'HDR', link)
    else:
        if 'Assets' not in bpy.context.scene.collection.children.keys():
            asset_coll = bpy.data.collections.new('Assets')
            context.scene.collection.children.link(asset_coll)
        else:
            asset_coll = bpy.data.collections['Assets']
        coll_name = os.path.splitext(
            os.path.basename(selected_preview))[0].title()
        obj_coll = bpy.data.collections.new(coll_name)
        asset_coll.children.link(obj_coll)
        append_element(selected_preview, 'Object', link)
        for obj in context.selected_objects:
            # obj.instance_collection = obj_coll
            obj_coll.objects.link(obj)
            context.collection.objects.unlink(obj)
        if context.scene.asset_manager.origin:
            cur_loc = context.scene.cursor_location
            bpy.ops.transform.translate(value=cur_loc)
            if context.scene.asset_manager.incl_cursor_rot:
                # TODO: use quaternion in ops.translate.rotate
                # or do each object separately
                pass


class SAM_LinkButton(bpy.types.Operator):
    bl_idname = "asset_manager.link_object"
    bl_label = "Add Object"

    def execute(self, context):
        execute_insert(context, link=True)
        return{'FINISHED'}


class SAM_AppendButton(bpy.types.Operator):
    bl_idname = "asset_manager.append_object"
    bl_label = "Add Object"

    def execute(self, context):
        execute_insert(context, link=False)
        return{'FINISHED'}


class SAM_OpenButton(bpy.types.Operator):
    bl_idname = "asset_manager.open_file"
    bl_label = "Open File"

    def execute(self, context):
        # TODO: open as searate instance of blender
        selected_preview = bpy.data.window_managers["WinMan"].asset_manager_prevs
        bpy.ops.wm.open_mainfile(filepath=selected_preview)
        return{'FINISHED'}


# Update
def update_category(self, context):
    enum_previews_from_directory_items(self, context)


def subcategory_callout(self, context):
    pref = context.preferences.addons[__name__].preferences
    path = os.path.join(pref.lib_path, self.cat)
    if self.cat == '.':
        return [('', '', '', 0), ]
    for r, d, f in os.walk(path):
        items = [(x, x, '', nr + 1) for nr, x in enumerate(d)]
        items.insert(0, ('.', '.', '', 0))
        return items if len(items) > 1 else [('', '', '', 0), ]


def categories(self, context):
    categories = []
    pref = context.preferences.addons[__name__].preferences
    for el in os.listdir(pref.lib_path):
        p = os.path.join(pref.lib_path, el)
        if os.path.isdir(p) and not el.startswith('.'):
            nr = len(categories)
            categories.append((el, el, '', nr + 1))
    categories.insert(0, ('.', '.', '', 0))
    return categories if len(categories) > 1 else [('', '', '', 0), ]


# Drop Down Menu
class SimpleAssetManager(bpy.types.PropertyGroup):

    cat: bpy.props.EnumProperty(
        items=categories,
        name="Category",
        description="Select a Category",
        update=update_category)

    subcat: bpy.props.EnumProperty(
        items=subcategory_callout,
        name="Subcategory",
        description="Select subcategory",
        update=update_category)

    origin: bpy.props.BoolProperty(
        name='Origin',
        description='Placement location')

    incl_cursor_rot: bpy.props.BoolProperty(
        name='Include cursor rotation',
        description='Includes cursor rotation on import.')


def enum_previews_from_directory_items(self, context):
    # Get the Preview Collection (defined in register func)
    pcoll = preview_collections["main"]

    pref = context.preferences.addons[__name__].preferences
    category = context.scene.asset_manager.cat
    subcategory = context.scene.asset_manager.subcat
    if category == '':
        directory = pref.lib_path
    elif subcategory == '':
        directory = os.path.join(pref.lib_path, category)
    else:
        directory = os.path.join(pref.lib_path, category, subcategory)

    # EnumProperty Callback
    enum_items = []

    if context is None:
        return enum_items

    wm = context.window_manager

    if directory == pcoll.asset_manager_prev_dir:
        return pcoll.asset_manager_prevs

    print("Scanning directory: %s" % directory)
    if directory and os.path.exists(directory):
        # Scan the Directory for blends
        empty_path = os.path.join(os.path.dirname(__file__), 'empty.png')
        image_paths = []
        dirs = []
        for fn in os.listdir(directory):
            if fn.lower().endswith(FORMATS):
                png = fn.rsplit('.', 1)[0] + '.png'
                if fn.lower().endswith(('.hdr', '.exr')):
                    image_paths.append((fn, True))
                elif png in os.listdir(directory):
                    image_paths.append((fn, True))
                else:
                    image_paths.append((fn, False))
            elif os.path.isdir(os.path.join(directory, fn)):
                dirs.append(fn)
        # append each folder with default folder icon, for optional folder searching
        # for i, name in enumerate(dirs):
        #     folder_thumb_path = ''
        #     filepath = os.path.join(directory, name)
        #     name = name.rsplit('.', 1)[0].replace('.', ' ').replace('_', ' ')
        #     name = name.lower().capitalize()
        #     enum_items.append((filepath, name, "", "FILE_FOLDER", i))

        # For each image in the directory, load the thumb
        # unless it has already been loaded
        nr = len(enum_items)
        for i, im in enumerate(image_paths):
            name, prev = im
            # Generate a Thumbnail Preview for a File.
            filepath = os.path.join(directory, name)
            name = name.rsplit('.', 1)[0].replace('.', ' ').replace('_', ' ')
            name = name.lower().capitalize()
            if filepath in pcoll:
                enum_items.append((filepath, name,
                                   "", pcoll[filepath].icon_id, nr + i))
            else:
                if prev:
                    imgpath = filepath.rsplit('.', 1)[0] + '.png'
                    if filepath.endswith(('.hdr', '.exr')):
                        imgpath = filepath
                    thumb = pcoll.load(filepath, imgpath, 'IMAGE')
                else:
                    thumb = pcoll.load(filepath, empty_path, 'IMAGE')
                enum_items.append((filepath, name,
                                   "", thumb.icon_id, nr + i))

    pcoll.asset_manager_prevs = enum_items
    pcoll.asset_manager_prev_dir = directory
    return pcoll.asset_manager_prevs


# Panel
class AssetPanel(bpy.types.Panel):
    # Create a Panel in the Tool Shelf
    bl_label = "Simple Asset Manager"
    bl_idname = "IMPORT_PT_Asset_Manager"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "View"
    bl_options = {"DEFAULT_CLOSED"}

    # Draw
    def draw(self, context):
        layout = self.layout
        wm = context.window_manager
        manager = context.scene.asset_manager
        # Categories Drop Down Menu
        col = layout.column()
        if manager.cat != '' or len(manager.cat) > 1:
            col.prop(manager, "cat")
        if manager.subcat != '' or len(manager.subcat) > 1:
            col.prop(manager, "subcat")
        # Previews
        if wm.asset_manager_prevs != '':
            row = layout.row()
            row.template_icon_view(wm, "asset_manager_prevs", show_labels=True)

            # Add Button
            row = layout.row()
            origin_btn_name = 'At Origin'
            if manager.origin:
                origin_btn_name = 'At Cursor'
            row.prop(manager, "origin", text=origin_btn_name, toggle=True)
            row = layout.row()
            row.operator("asset_manager.append_object", text="Append")
            if wm.asset_manager_prevs.endswith('.blend'):
                row = layout.row()
                row.operator("asset_manager.open_file", text="Open")
                row.operator("asset_manager.link_object", text="Link")


class AssetPrefPanel(bpy.types.AddonPreferences):
    bl_idname = __name__

    lib_path: bpy.props.StringProperty(
        name="Library Path",
        default=os.path.splitdrive(__file__)[0],
        description="Show only hotkeys that have this text in their name",
        subtype="DIR_PATH")

    rerender: bpy.props.BoolProperty()

    incl_cursor_rot: bpy.props.BoolProperty(
        name='Include cursor rotation',
        description='Include rotation when appending to cursor.')

    def draw(self, context):
        layout = self.layout
        col = layout.column()
        col.prop(self, "lib_path", text='Library path')
        # col.prop(self, "incl_cursor_rot")
        row = layout.row()
        row.operator("asset_manager.render_previews",
                     text="Render missing previews")
        row.prop(self, 'rerender',
                 text='Re-render ALL previews')


preview_collections = {}

#####################################################################
# Register

classes = (
    SAM_render_previews,
    SAM_LinkButton,
    SAM_AppendButton,
    SAM_OpenButton,
    SimpleAssetManager,
    AssetPanel,
    AssetPrefPanel,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    WindowManager.asset_manager_prev_dir = StringProperty(
        name="Folder Path",
        subtype='DIR_PATH',
        default="")

    WindowManager.asset_manager_prevs = EnumProperty(
        items=enum_previews_from_directory_items)

    pcoll = bpy.utils.previews.new()
    pcoll.asset_manager_prev_dir = ""
    pcoll.asset_manager_prevs = ""

    preview_collections["main"] = pcoll
    bpy.types.Scene.asset_manager = bpy.props.PointerProperty(
        type=SimpleAssetManager)


# Unregister
def unregister():
    del WindowManager.asset_manager_prevs

    for pcoll in preview_collections.values():
        bpy.utils.previews.remove(pcoll)
    preview_collections.clear()

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.asset_manager


if __name__ == "__main__":
    register()
